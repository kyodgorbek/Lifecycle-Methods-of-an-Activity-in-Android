# Lifecycle-Methods-of-an-Activity-in-Android
protected void onCreate(Bundle savedInstanceState);
protected void onStart();

protected void onStart();
protected void onResume();
protected void onPause();
protected void onStop();
protected void onDestroy();
